import ListElement, {IListElement, ListElementSchema} from "./ListElement";
import mongoose, {Schema, Document} from "mongoose";

export interface IToDoList extends Document{
    name: string;
    listElements: [IListElement];
}

const ToDoListSchema = new Schema({
    name:{
        type: String,
        required: true,
        unique: true,
        index: true
    },

    listElements:{
        type: ListElement,
        //de completat :D
    }
});

export default mongoose.model<IToDoList>("ToDoList", ToDoListSchema);



