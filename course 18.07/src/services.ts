import mongoose from "mongoose";
import ToDoList from "./models/ToDoList"
import ListElement from "./models/ListElement";

export class DriverManager{
    private MONGO_STRING: string = "mongodb://localhost:27017/JavaScriptCourse";
    private static instance: DriverManager;

    private constructor(){}

    public connect(){
        mongoose.connect(this.MONGO_STRING, {
            useNewUrlParser: true,
            useFindAndModify: true,
            useCreateIndex: true
        });
    
    let db = mongoose.connection;
    db.once("open", () => {
        console.log("Connected to" + this.MONGO_STRING);
    })
}
   public static get Instance(){
        if(this.instance){
            return this.instance;
        } else{
            this.instance = new DriverManager();
            return this.instance;
        }
    }

    public async addNewList(listName: string){
        let newList = new ToDoList({
            name: listName
        });

        return await newList.save();
    }

    public async addNewElementForList(
        listName: string,
        taskName: string,
        taskDescription: string
        ){
        let newItem = new ListElement({
            title: taskName,
            body: taskDescription
        });
       /* return ToDoList.findOneAndUpdate({
            name: listName,
        });*/
    }
    
}